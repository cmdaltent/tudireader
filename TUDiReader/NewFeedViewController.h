//
//  NewFeedViewController.h
//  TUDiReader
//
//  Created by Martin Weißbach on 10/28/13.
//  Copyright (c) 2013 Technische Universität Dresden. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
    Controls the NewFeedView. 
 */
@interface NewFeedViewController : UIViewController

@property (nonatomic) NSManagedObjectContext *managedObjectContext;

@end
