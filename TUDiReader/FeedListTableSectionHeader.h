//
//  FeedListTableSectionHeader.h
//  TUDiReader
//
//  Created by Martin Weissbach on 21/12/13.
//  Copyright (c) 2013 Technische Universität Dresden. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedListTableSectionHeader : UIView

@property (weak, nonatomic) IBOutlet UILabel *title;

@end
