//
//  NavigationViewController.h
//  TUDiReader
//
//  Created by Martin Weissbach on 21/12/13.
//  Copyright (c) 2013 Technische Universität Dresden. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
    Decorates UINavigationController with a custom navigation bar styling.
    Prevailes all other navigation controller behaviors.
 */
@interface NavigationViewController : UINavigationController

@end
