//
//  WebViewController.h
//  TUDiReader
//
//  Created by Martin Weißbach on 12/2/13.
//  Copyright (c) 2013 Technische Universität Dresden. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

- (id)initWithURL:(NSURL *)url;

@end
